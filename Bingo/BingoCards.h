//
// Created by a01475585 on 3/3/2017.
//

#ifndef BINGO_BINGOCARDS_H
#define BINGO_BINGOCARDS_H


class BingoCards {
#include <ostream>
#include <vector>

public:
    int indexNumber;
    int width;
    int size;
    int getWidth(size);

};


#endif //BINGO_BINGOCARDS_H
