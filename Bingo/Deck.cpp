//
// Created by Stephen Clyde on 2/16/17.
//

#include "Deck.h"
#include <vector>
#include <iostream>
#include <cmath>

Deck::Deck(int cardSize, int cardCount, int numberMax,  std:: vector<int> &numbers)
{

    //starting vector
    srand (time(NULL));
    for(int j = 0; j < cardSize * cardSize; j++) {

        int i;
        i = (rand() % numberMax + 1);
        numbers.push_back(i);
        
    }


}

Deck::~Deck()
{
    // TODO: Implement
    //delete;
}

void Deck::print(std::ostream& out, std:: vector<int> &numbers) const
{
    int size = numbers.size();
    int width = sqrt(size);
    int j = 0;

    for(int i = 0; i < width; i++) {

        for (int i = 0; i < width; i++) {
            std::cout << "+----";
        }
        std::cout << "+" << std::endl;
        for (int i = 0; i < width; i++) {
            std::cout << "| " << numbers[j] << " ";
                if(numbers[j] < 10) std:: cout << " ";
            j++;
        }
        std::cout << "|" << std::endl;
    }
    for (int i = 0; i < width; i++) {
        std::cout << "+----";
    }
    std::cout << "+" << std::endl;

    
}


void Deck::print(std::ostream& out, int cardIndex) const
{
   
}



