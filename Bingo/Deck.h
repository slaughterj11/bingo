//
// Created by Stephen Clyde on 2/16/17.
//

#ifndef BINGO_DECK_H
#define BINGO_DECK_H

#include <ostream>
#include <vector>

// TODO: Extend this definition as you see fit

class Deck {

public:
    Deck(int cardSize, int cardCount, int numberMax,  std:: vector<int> &numbers);
    ~Deck();

    void print(std::ostream& out, std:: vector<int> &numbers) const;
    void print(std::ostream& out, int cardIndex) const;
};

#endif //BINGO_DECK_H
