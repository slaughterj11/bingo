//
// Created by Stephen Clyde on 2/16/17.
//

#ifndef BINGO_USER_INTERFACE_H
#define BINGO_USER_INTERFACE_H

#include <string>
#include <vector>

class Deck;

class UserInterface {
private:
    Deck*   currentDeck = nullptr;

public:
    void run();

private:
    void createDeck();
    void deckMenu(std:: vector<int> &numbers, int cardCount);
    void printCard(std:: vector<int> &numbers);
    void saveDeck(std:: vector<int> &numbers);

    std::string getStringInput(std::string prompt);
    int getNumberInput(std::string prompt, int rangeMin, int rangeMax);
};


#endif //BINGO_USER_INTERFACE_H
